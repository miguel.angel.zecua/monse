<!-- latest jquery-->
<script src="<?php echo base_url('public/assets/js/jquery-3.5.1.min.js') ?>"></script>
    <!-- feather icon js-->
<script src="<?php echo base_url('public/assets/js/icons/feather-icon/feather.min.js') ?>"></script>

<script src="<?php echo base_url('public/assets/js/icons/feather-icon/feather-icon.js') ?>"></script>
    <!-- Sidebar jquery-->
<script src="<?php echo base_url('public/assets/js/sidebar-menu.js') ?>"></script>

<script src="<?php echo base_url('public/assets/js/config.js') ?>"></script>
    <!-- Bootstrap js-->
<script src="<?php echo base_url('public/assets/js/bootstrap/popper.min.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/bootstrap/bootstrap.min.js') ?>"></script>
    <!-- Plugins JS start-->
<script src="<?php echo base_url('public/assets/js/prism/prism.min.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/clipboard/clipboard.min.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/counter/jquery.waypoints.min.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/counter/jquery.counterup.min.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/counter/counter-custom.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/custom-card/custom-card.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/datepicker/date-picker/datepicker.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/datepicker/date-picker/datepicker.en.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/datepicker/date-picker/datepicker.custom.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/owlcarousel/owl.carousel.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/general-widget.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/height-equal.js') ?>"></script>
    
<script src="<?php echo base_url('public/assets/js/tooltip-init.js') ?>"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
<script src="<?php echo base_url('public/assets/js/script.js') ?>"></script>
    
    <script src="<?php echo base_url('public/assets/js/theme-customizer/customizer.js') ?>"></script>
    <!-- login js-->
    <!-- Plugin used-->